# 1
# 函数parse_model，从yaml文件中深copy出要搭建的字典的key值，被2调用

# 2
# 类Model，搭建神经网络，只有最简单的__init__和前向传播forward


import sys
import math
import yaml
import argparse

sys.path.append('./')
from common import *
from utils.general import check_file


# 1 函数parse_model

def parse_model(yaml):
    anchors, nc = yaml['anchors'], yaml['nc']
    depth_multiple, width_multiple = yaml['depth_multiple'], yaml['width_multiple']
    num_anchors = (len(anchors[0]) // 2) if isinstance(anchors, list) else anchors
    output_dims = num_anchors * (nc + 5)

    layers = []
    # from, number, module, args
    for i, (f, number, module, args) in enumerate(yaml['backbone'] + yaml['head']):
        # all component is a Class, initialize here, call in self.forward
        module = eval(module) if isinstance(module, str) else module

        for j, arg in enumerate(args):
            try:
                args[j] = eval(arg) if isinstance(arg, str) else arg  # eval strings, like Detect(nc, anchors)
            except:
                pass

        number = max(round(number * depth_multiple), 1) if number > 1 else number  # control the model scale

        if module in [Conv, RepConv, SPPCSPC]:
            c2 = args[0]
            c2 = math.ceil(c2 * width_multiple / 8) * 8 if c2 != output_dims else c2
            args = [c2, *args[1:]]

            if module in [SPPCSPC]:
                args.insert(1, number)
                number = 1

        modules = tf.keras.Sequential(*[module(*args) for _ in range(number)]) if number > 1 else module(*args)
        modules.i, modules.f = i, f

        print('%3s %18s %3s   %-40s%-30s' % (i, f, number, module, args))
        layers.append(modules)
    return layers


# 2 类Model

class Model(object):
    def __init__(self, yaml_dir):
        with open(yaml_dir, 'rb') as f:
            yaml_dict = yaml.load(f, Loader=yaml.FullLoader)
        self.module_list = parse_model(yaml_dict)
        module = self.module_list[-1]
        if isinstance(module, IDetect):
            # transfer the anchors to grid coordinator, 3 * 3 * 2
            module.anchors /= tf.reshape(module.stride, [-1, 1, 1])

    def __call__(self, img_size, name='yolo'):
        x = tf.keras.Input([img_size, img_size, 3])
        output = self.forward(x)  # 用call来自动进行下面的forward前向传播
        return tf.keras.Model(inputs=x, outputs=output, name=name)

    def forward(self, x):
        y = []
        for module in self.module_list:
            if module.f != -1:  # if not from previous layer
                if isinstance(module.f, int):
                    x = y[module.f]
                else:
                    x = [x if j == -1 else y[j] for j in module.f]

            x = module(x)
            y.append(x)
        return x


if __name__ == '__main__':
    parser = argparse.ArgumentParser()  #
    parser.add_argument('--cfg', type=str, default='../cfg/yolov7_tf.yaml', help='model.yaml')
    parser.add_argument('--device', default='', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
    parser.add_argument( '--profile', action='store_true', help='profile model speed')
    opt = parser.parse_args()
    opt.cfg = check_file(opt.cfg)  # check file在general.py中的143行

    # Create model
    model = Model(opt.cfg)

    # test_tf
    img_tf = tf.random.normal(shape=[1, 64, 64, 3])
    y = model.forward(img_tf)


    print(len(y))
    # list:3; 0=tensor:{1,3,8,8,85}, 1=tensor:{1,3,4,4,85}, 2=tensor:{1,3,2,2,85}
    print(y[0].shape, y[1].shape, y[2].shape)





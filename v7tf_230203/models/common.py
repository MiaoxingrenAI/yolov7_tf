# 这里目前需要配合yaml文件，一些必要的tf搭建的组件


import os
import numpy as np

import tensorflow as tf
from tensorflow.python.keras.layers import Layer, Conv2D, MaxPooling2D, MaxPool2D
from tensorflow.keras.layers import BatchNormalization
from tensorflow.python.keras.activations import silu, swish
from tensorflow.python.keras import Model, Sequential
from tensorflow.python.keras import backend as K

# 1Conv, 2MP, 3Concat, 4SPPCSPC, 5Upsample, 6RepConv, 7IDetect

# 1 Conv

class Conv(Layer):
    def __init__(self, filters, kernel_size, strides, padding='same', groups=1):
        super(Conv, self).__init__()
        self.conv = Conv2D(filters, kernel_size, strides, padding, groups=groups, use_bias=False,
                           kernel_initializer=tf.random_normal_initializer(stddev=0.01),
                           kernel_regularizer=tf.keras.regularizers.L2(5e-4))
        self.bn = BatchNormalization()
        self.act = silu

    def call(self, x):

        # conv_mean = tf.reduce_mean(self.conv(x))
        # print(conv_mean)
        # bn = self.bn(self.conv(x))
        # bn_mean = tf.reduce_mean(bn)
        # print(bn_mean)
        # print('')
        return self.act(self.bn(self.conv(x)))

    def fuseforward(self, x):
        return self.act(self.conv(x))


# 2 MP

class MP(Layer):
    def __init__(self, k=2):
        super(MP, self).__init__()
        self.m = MaxPooling2D(pool_size=k, strides=k)

    def call(self, x):
        return self.m(x)


# 3 Concat

class Concat(Layer):
    def __init__(self, dims=-1):
        super(Concat, self).__init__()
        self.d = dims

    def call(self, x):
        return tf.concat(x, self.d)


# 4 SPPCSPC

class SPPCSPC(Layer):
    # Cross Stage Partial Networks
    def __init__(self, filters, n=1, shortcut=False, expansion=0.5, strides=(5, 9, 13)):
        super(SPPCSPC, self).__init__()
        filters_expan = int(2 * filters * expansion)
        self.conv1 = Conv(filters_expan, 1, 1)
        self.conv2 = Conv2D(filters_expan, 1, 1, use_bias=False,
                            kernel_initializer=tf.random_normal_initializer(stddev=0.01))
        self.conv3 = Conv(filters_expan, 3, 1)
        self.conv4 = Conv(filters_expan, 1, 1)
        self.modules = [MaxPool2D(pool_size=x, strides=1, padding='same') for x in strides]
        self.conv5 = Conv(filters_expan, 1, 1)
        self.conv6 = Conv(filters_expan, 3, 1)
        self.bn = BatchNormalization()
        self.act = silu
        self.conv7 = Conv(filters, 1, 1)

    def call(self, x):
        x1 = self.conv4(self.conv3(self.conv1(x)))
        y1 = self.conv6(self.conv5(tf.concat([x1] + [module(x1) for module in self.modules], axis=-1)))
        y2 = self.conv2(x)
        return self.conv7(self.act(self.bn(tf.concat([y1, y2], axis=-1))))


# 5 Upsample

class Upsample(Layer):
    def __init__(self, i=None, ratio=2, method='bilinear'):
        super(Upsample, self).__init__()
        self.ratio = ratio
        self.method = method

    def call(self, x):
        return tf.image.resize(x, (tf.shape(x)[1] * self.ratio, tf.shape(x)[2] * self.ratio), method=self.method)


# 6 RepConv
# 来自林梓鑫
class RepConv(Layer):
    def __init__(self, c2, k=3, s=1, p=None, g=1, act=True, deploy=False):
        super(RepConv, self).__init__()

        self.deploy = deploy
        self.groups = g
        # self.in_channels = c1
        self.s = s
        self.out_channels = c2

        assert k == 3
        # assert autopad(k, p) == 1

        # padding_11 = autopad(k, p) - k // 2

        # self.act = SiLU() if act is True else (act if isinstance(act, Layer) else Identity())
        self.act = silu

        if deploy:
            self.rbr_reparam = Conv2D(c2, k, s, padding='same', groups=g, use_bias=True)

        else:
            # self.rbr_identity = BatchNormalization() if c2 == c1 and s == 1 else None

            self.rbr_dense = Sequential([
                Conv2D(c2, k, s, padding='same', groups=g, use_bias=False),
                BatchNormalization()
            ]
            )

            self.rbr_1x1 = Sequential([
                Conv2D(c2, 1, s, padding='same', groups=g, use_bias=False),
                BatchNormalization()
            ]
            )

    def call(self, inputs, **kwargs):
        _, _, _, input_channels = inputs.shape
        if hasattr(self, "rbr_reparam"):
            return self.act(self.rbr_reparam(inputs))

        if input_channels == self.out_channels and self.s == 1:
            id_out = BatchNormalization()(inputs)
        else:
            id_out = 0

        return self.act(self.rbr_dense(inputs) + self.rbr_1x1(inputs) + id_out)



# 7 IDetect
# 来自佟瑞升
class IDetect(Layer):
    def __init__(self, num_classes, anchors=()):
        super(IDetect, self).__init__()
        self.num_classes = num_classes
        self.num_scale = 3
        self.output_dims = self.num_classes + 5
        self.num_anchors = len(anchors[0]) // 2
        self.stride = np.array([8, 16, 32], np.float32)  # fixed here, modify if structure changes
        self.anchors = tf.cast(tf.reshape(anchors, [self.num_anchors, -1, 2]), tf.float32)
        self.modules = [Conv2D(self.output_dims * self.num_anchors, 1, use_bias=False) for _ in range(self.num_scale)]

    def call(self, x, training=True): # x0: 1,8,8,256; x1: 1,4,4,256; x2: 1,2,2,1024
        res = []
        for i in range(self.num_scale):  # number of scale layer, default=3
            y = self.modules[i](x[i])
            _, grid1, grid2, _ = y.shape
            y = tf.reshape(y, (-1, grid1, grid2, self.num_scale, self.output_dims))

            grid_xy = tf.meshgrid(tf.range(grid1), tf.range(grid2))  # grid[x][y]==(y,x)
            grid_xy = tf.cast(tf.expand_dims(tf.stack(grid_xy, axis=-1), axis=2), tf.float32)

            y_norm = tf.sigmoid(y)  # sigmoid for all dims # 都变成了0.5
            xy, wh, conf, classes = tf.split(y_norm, (2, 2, 1, self.num_classes), axis=-1)

            pred_xy = (xy * 2. - 0.5 + grid_xy) * self.stride[i]  # decode pred to xywh
            pred_wh = (wh * 2) ** 2 * self.anchors[i] * self.stride[i]

            out = tf.concat([pred_xy, pred_wh, conf, classes], axis=-1)
            res.append(out)
        return res


import math
from copy import deepcopy
import tensorflow as tf

def copy_attr(a, b, include=(), exclude=()):
    """在ModelEMA函数和yolo.py中Model类的autoshape函数中调用
    复制b的属性(这个属性必须在include中而不在exclude中)给a
    :params a: 对象a(待赋值)
    :params b: 对象b(赋值)
    :params include: 可以赋值的属性
    :params exclude: 不能赋值的属性
    """
    # Copy attributes from b to a, options to only include [...] and to exclude [...]
    # __dict__返回一个类的实例的属性和对应取值的字典
    for k, v in b.__dict__.items():
        if (len(include) and k not in include) or k.startswith('_') or k in exclude:
            continue
        else:
            # 将对象b的属性k赋值给a
            setattr(a, k, v)

class ModelEMA:
    """
    用在train.py中的test.run（测试）阶段
    模型的指数加权平均方法(Model Exponential Moving Average)
    是一种给予近期数据更高权重的平均方法 利用滑动平均的参数来提高模型在测试数据上的健壮性/鲁棒性 一般用于测试集
    https://www.bilibili.com/video/BV1FT4y1E74V?p=63
    https://www.cnblogs.com/wuliytTaotao/p/9479958.html
    https://zhuanlan.zhihu.com/p/68748778
    https://zhuanlan.zhihu.com/p/32335746
    https://github.com/ultralytics/yolov5/issues/608
    https://github.com/rwightman/pytorch-image-models/blob/master/timm/utils/model_ema.py
     Model Exponential Moving Average from https://github.com/rwightman/pytorch-image-models
    Keep a moving average of everything in the model state_dict (parameters and buffers).
    This is intended to allow functionality like
    https://www.tensorflow.org/api_docs/python/tf/train/ExponentialMovingAverage
    A smoothed version of the weights is necessary for some training schemes to perform well.
    This class is sensitive where it is initialized in the sequence of model init,
    GPU assignment and distributed training wrappers.
    """

    def __init__(self, model, decay=0.9999, updates=0):
        """train.py
        model:
        decay: 衰减函数参数
               默认0.9999 考虑过去10000次的真实值
        updates: ema更新次数
        """
        # 创建ema模型  Create EMA

        ###is_parallel(model)
        self.ema = deepcopy(model.module).eval()  # FP32 EMA
        # if next(model.parameters()).device.type != 'cpu':
        #     self.ema.half()  # FP16 EMA
        self.updates = updates  # number of EMA updates
        # self.decay: 衰减函数 输入变量为x decay exponential ramp (to help early epochs)
        self.decay = lambda x: decay * (1 - math.exp(-x / 2000))
        # 所有参数取消设置梯度(测试  model.val)
        for p in self.ema.parameters():
            p.requires_grad_(False)

    def update(self, model):
        # Update EMA parameters
        # with是python中上下文管理器，简单理解，当要进行固定的进入，返回操作时，可以将对应需要的操作，放在with所需要的语句中
        # 将with后的代码块运行之后,对代码块进行close操作。

        #对于tensor的计算操作，默认是要进行计算图的构建的,以便梯度反向传播等操作
        #并不是所有的操作都需要进行计算图的构建.
        #使用 with torch.no_grad(),强制之后的内容不进行计算图构建,对代码块中的tensor设置requires_grad=False
        # 这最常用于评估和测试循环，其中在推理后不期望反向传播.也可以在训练期间使用，例如在对冻结组件进行推断并且不需要更新梯度时
        # with torch.no_grad():
        self.updates += 1 # ema更新次数 + 1
        d = self.decay(self.updates) # 随着更新次数 更新参数贝塔(d)
        # msd: 模型配置的字典 model state_dict  msd中的数据保持不变 用于训练
        msd = model.module.state_dict() if is_parallel(model) else model.state_dict()  # model state_dict
        # 遍历模型配置字典 如: k=linear.bias  v=[0.32, 0.25]  ema中的数据发生改变 用于测试
        for k, v in self.ema.state_dict().items():
            tf.stop_gradient(v)  # #修改，只有v为tensor类型，需要设置为不可更新
            # 这里得到的v: 预测值
            if v.dtype.is_floating_point:
                v *= d  #公式左边 decay * shadow_variable
                # .detach() 使对应的Variables与网络隔开而不参与梯度更新
                v += (1. - d) * msd[k].detach() # 公式右边  (1−decay) * variable

    def update_attr(self, model, include=(), exclude=('process_group', 'reducer')):
        # Update EMA attributes
        # 调用上面的copy_attr函数 从model中复制相关属性值到self.ema中
        copy_attr(self.ema, model, include, exclude)


#修改-设置变量不参与梯度更新
#变量少的时候，直接对变量a设置trainable=False，或者使用tf.stop_gradient(a)使其不参与梯度更新
#变量多的时候，可使用列表进行设置
# pytorch中with torch.no_grad() https://blog.csdn.net/weixin_42769131/article/details/113106280
# tensorflow设置变量不参与梯度更新：https://www.cnblogs.com/hrlnw/p/10400057.html


